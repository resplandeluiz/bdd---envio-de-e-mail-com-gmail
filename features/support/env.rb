require 'capybara'
require 'capybara/cucumber'
require 'site_prism'
require 'selenium/webdriver'
require 'rspec'


Selenium::WebDriver::Chrome.driver_path = 'C:\Users\Luiz Resplande\Downloads\chromedriver_win32\chromedriver.exe'

Capybara.configure do |config|  
  config.default_driver = :selenium_chrome # Com Navegador
  #config.default_driver = :selenium_chrome_headless # Sem navegador
  config.app_host = 'https://mail.google.com/mail/' #CONFIG['url_padrao']
  Capybara.default_max_wait_time = 15
end
