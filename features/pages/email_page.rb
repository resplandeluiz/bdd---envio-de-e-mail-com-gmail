
class EmailPage < SitePrism::Page
    
    include Capybara::DSL   
    
    element :button_open_box_mail, "div[gh=cm]"
    element :email_to, "textarea[name=to]"
    element :input_assunto, "input[name=subjectbox]"
    element :text_content, "div[aria-label='Corpo da mensagem']"
    element :button_submit, "div[role='button']", :text=>'Enviar'
 

    def open_box_mail
      
      button_open_box_mail.click
      email_to.set 'EMAIL_TO'
      input_assunto.set 'Email enviado pelo Capybara'
      text_content.set 'Testando a nossa habilidade'
      
      
      button_submit.click
      
      sleep 5
    end
    
    
        
  
  end
 