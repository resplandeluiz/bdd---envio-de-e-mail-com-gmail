class LoginPage < SitePrism::Page
    
    include Capybara::DSL   
    
    element :login_email, :id,"identifierId"
    element :password,  "input[name=password]"
    element :button_proxima, 'span .RveJvd', :text=>'Próxima'

    def set_email
      
      login_email.set 'MYEMAIL'
      button_proxima.click     
      
      password.set 'MYPASSWORD'
      button_proxima.click          
    end
    
    
        
  
  end