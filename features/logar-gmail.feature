#language:pt

  Funcionalidade: Enviar um e-mail como usuário do Gmail

    Como um usuário do GMail
    Quero enviar um e-mail para o meu própio e-mail
    Para testar meus conhecimentos

    Contexto: Acessar a tela principal do GMail

      @enviar-email
      Cenário: Enviar um e-mail para eu própio

        Dado que eu já esteja logado no gmail
        Quando eu clico em novo compose e preencho os campos e clico em submit
        Então devo receber um e-mail
