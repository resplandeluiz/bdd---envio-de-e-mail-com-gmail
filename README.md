BDD - Behavior Drive Development

 - Desenvolvido a fins acadêmicos, este agloritmo realiza o acesso ao gmail e envia um e-mail para algum usuário selecionado.

_________________________________________________________


Primeiro vá em **features/pages/pagina_inicial.rb**

Substituia
    ` MYEMAIL  e MYPASSWORD` , pelo seu e-mail e senha

Segundo vá em **/features/pages/email_page.rb**

Coloque o e-mail da pessoa que irá receber
    ` EMAIL_TO ` pelo e-mail do destinatário

Após tudo isso vá em **/features/support/env.rb**
    Verifique sua versão do chrome, é o navegador utilizado neste teste.
    ` Selenium::WebDriver::Chrome.driver_path ` , baixe o chromedriver igual ao da sua versão instalada e coloque o caminho que você baixou!
_________________________________________________________

Utilizando
   
   - Cabypara 
   - Cucumber
   - Ruby

_________________________________________________________

Desenvolvido por **Luiz Resplande**

